## SWAN: Short-Wavelength stability ANalysis##

SWAN is a numerical code written by Dr Jérémie Vidal (https://sites.google.com/view/jvidalhome). The code is realeased under the [CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible with GNU GPL). 

FEATURES:
---------

The code probes the **linear stability** of **arbitrary** basic states, expressed in **Cartesian** coordinates (but not necessarily linear in Cartesian space coordinates). 
It is based upon the pioneering works of [Lifschitz & Hameiri (1991)](http://aip.scitation.org/doi/abs/10.1063/1.858153) & [Friedlander & Vishik (1991)](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.66.2204).
SWAN has been **benchmarked** against previously published local stability results. This method only gives **sufficient** conditions for **local instability**.

Linear perturbations are expanded onto Lagrangian **short-wavelength** perturbations, advected along the **Lagrangian** fluid **trajectories** of the basic flow.  The local stability equations are expressed in Lagrangian description in an arbitrary **rotating** frame. SWAN accounts for stability equations for double-diffusive **Boussinesq** fluids, extending [Kirillov & Mutabazi (2017)](https://www.cambridge.org/core/journals/journal-of-fluid-mechanics/article/shortwavelength-local-instabilities-of-a-circular-couette-flow-with-radial-temperature-gradient/6A17081582CA83FF556C283063BE4CA1) and **magnetic fields** (under some constraints), extending [Mizerski & Bajer (2011)](http://www.sciencedirect.com/science/article/pii/S0167278911000388). Note that hydrodmagnetic equations differ from [Friendlander & Vishik (1995)](http://aip.scitation.org/doi/abs/10.1063/1.166112) and [Kirillov et al. (2014)](https://www.cambridge.org/core/journals/journal-of-fluid-mechanics/article/local-instabilities-in-magnetized-rotational-flows-a-shortwavelength-approach/AB60D02DCA41FE9B5EE2319070CF0EB6), because they reduce to **ODE** along the Lagrangian fluid trajectories (as in the pure hydrodynamic case). 

* Written in **Python 3**. Arbitrary basic states (both linear and nonlinear) and parameters can be changed in a user-defined file params.py to tackle a wide range of problems (see the examples).
* Designed for **speed**. Stability equations are built symbolically using [Sympy](http://www.sympy.org/fr/), then converted to a Fortran subroutine with the Sympy fcode function, and finally wrapped with [f2py](https://docs.scipy.org/doc/numpy-dev/f2py/) for fast numerical evaluations with [Numpy](http://www.numpy.org/). 
* **Parallel survey** of the parameter space. Because computations are **embarrassingly parallel**, SWAN uses **grid computations** on loop parameters and and **MPI** processes to loop over all possible initial wave vectors. SWAN can run either on a laptop or on massively parallel supercomputers.
* Time integrators: a **Floquet** solver for **periodic** configurations and **brute-force** solvers.

To use SWAN for research purposes, **please cite** 

* [Vidal & Cébron (JFM, 2017)](https://www.google.com/url?q=https%3A%2F%2Fdoi.org%2F10.1017%2Fjfm.2017.689&sa=D) for hydrodynamic computations,
* [Vidal et al. (A&A, 2019)](https://www.google.com/url?q=https%3A%2F%2Fdoi.org%2F10.1051%2F0004-6361%2F201935658&sa=D) with buoyancy effects.
 
USING SWAN:
-----------

- Download the .zip file in the `Downloads` tab.
- A fortran compiler (e.g. gfortran).
- Python 3 with [Numpy](http://www.numpy.org/), [Scipy](https://www.scipy.org/), [Sympy](http://www.sympy.org/fr/) and [mpi4py](https://pypi.python.org/pypi/mpi4py).

All the required Python libraries are included within [Anaconda](https://www.anaconda.com/download/#linux), a user-freiendly python distribution that does not require root access to be installed.


